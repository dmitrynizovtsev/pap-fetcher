task :fetch_affiliates => :environment do
  require 'papapi'

  def fetch_affiliates(session, offset, limit)
    aff_request = Papapi::GridRequest.new("Pap_Merchants_User_AffiliatesGridSimple", "getRows", session)
    aff_request.sort_col = "id"
    aff_request.sort_asc = false
    aff_request.offset = offset
    aff_request.limit = limit
    aff_request.add_column("id")
    aff_request.add_column("refid")
    aff_request.add_column("firstname")
    aff_request.add_column("lastname")
    aff_request.add_column("userid")
    aff_request.add_column("username")
    aff_request.add_column("note")
    aff_request.add_column("ip")
    aff_request.add_column("rstatus")
    aff_request.add_column("parentfirstname")
    aff_request.add_column("parentlastname")
    aff_request.add_column("parentuserid")
    aff_request.add_column("dateinserted")
    aff_request.add_column("dateapproved")
    aff_request.add_column("originalparentuserid")
    aff_request.add_column("data1")
    aff_request.add_column("data2")
    aff_request.add_column("data3")
    aff_request.add_column("data4")
    aff_request.add_column("data5")
    aff_request.add_column("data6")
    aff_request.add_column("data7")
    aff_request.add_column("data8")
    aff_request.add_column("data9")
    aff_request.add_column("data10")
    aff_request.add_column("data11")
    aff_request.add_column("data12")
    aff_request.add_column("payoutmethod")
    aff_request.add_column("payoutmethod")
    aff_request.add_column("lastlogin")
    aff_request.add_column("loginsnr")
    response = aff_request.send
    response
  end

  session = Papapi::Session.new("#{Rails.application.config.pap_server_url}/scripts/server.php",true).
      login(Rails.application.config.pap_merchant_login, Rails.application.config.pap_merchant_password)


  count_affs = 0
  offset = 0
  affiliates = []
  while true  do
    result = fetch_affiliates(session, offset, 30)
    count_affs = result.count
    result.each do |r|
      p r['id']
      affiliates.push(r)
    end

    if count_affs <= affiliates.length
      break
    else
      offset=offset + 30
    end
    sleep 5
  end
end