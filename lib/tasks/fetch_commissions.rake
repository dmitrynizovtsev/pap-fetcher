task :fetch_commissions => :environment do
  require 'papapi'

#  {"C":"Gpf_Rpc_Server", "M":"run", "requests":[{"C":"Pap_Merchants_Transaction_TransactionsGrid", "M"
#  :"getRows", "sort_col":"dateinserted", "sort_asc":false, "offset":0, "limit":30, "columns":[["id"],["id"
#  ],["commission"],["totalcost"],["fixedcost"],["t_orderid"],["productid"],["countrycode"],["dateinserted"
#  ],["dateapproved"],["banner"],["rtype"],["bannerid"],["name"],["campaignid"],["tier"],["commissionTypeName"
#  ],["commisionGroup"],["username"],["rstatus"],["payoutstatus"],["firstname"],["lastname"],["userid"]
#  ,["refid"],["ip"],["useragent"],["trackmethod"],["refererurl"],["recurringcommid"],["payouthistoryid"
#  ],["clickcount"],["firstclicktime"],["firstclickreferer"],["firstclickip"],["firstclickdata1"],["firstclickdata2"
#  ],["lastclicktime"],["lastclickreferer"],["lastclickip"],["lastclickdata1"],["lastclickdata2"],["data1"
#  ],["data2"],["data3"],["data4"],["data5"],["originalcurrencyid"],["original_currency_code"],["originalcurrencyrate"
#  ],["originalcurrencyvalue"],["merchantnote"],["systemnote"],["channel"],["payoutdate"],["loggroupid"
#  ],["actions"]]}], "S":"11885dc8cbcad426f811d557e1a88901"}

  def fetch_commissions(session, offset, limit)
    c_request = Papapi::GridRequest.new("Pap_Merchants_Transaction_TransactionsGrid", "getRows", session)
    c_request.sort_col = "dateinserted"
    c_request.sort_asc = false
    c_request.offset = offset
    c_request.limit = limit
    c_request.add_column("id")
    c_request.add_column("commission")
    c_request.add_column("totalcost")
    c_request.add_column("fixedcost")
    c_request.add_column("t_orderid")
    c_request.add_column("productid")
    c_request.add_column("countrycode")
    c_request.add_column("dateinserted")
    c_request.add_column("dateapproved")
    c_request.add_column("banner")
    c_request.add_column("rtype")
    c_request.add_column("bannerid")
    c_request.add_column("name")
    c_request.add_column("campaignid")
    c_request.add_column("tier")
    c_request.add_column("commissionTypeName")
    c_request.add_column("commisionGroup")
    c_request.add_column("username")
    c_request.add_column("rstatus")
    c_request.add_column("payoutstatus")
    c_request.add_column("firstname")
    c_request.add_column("lastname")
    c_request.add_column("userid")
    c_request.add_column("refid")
    c_request.add_column("ip")
    c_request.add_column("useragent")
    c_request.add_column("trackmethod")
    c_request.add_column("refererurl")
    c_request.add_column("recurringcommid")
    c_request.add_column("payouthistoryid")
    c_request.add_column("clickcount")

    c_request.add_column("firstclicktime")
    c_request.add_column("firstclickreferer")
    c_request.add_column("firstclickip")
    c_request.add_column("firstclickdata1")
    c_request.add_column("firstclickdata2")
    c_request.add_column("data1")
    c_request.add_column("data2")
    c_request.add_column("data3")
    c_request.add_column("data4")
    c_request.add_column("data5")
    c_request.add_column("originalcurrencyid")
    c_request.add_column("original_currency_code")
    c_request.add_column("originalcurrencyrate")
    c_request.add_column("originalcurrencyvalue")
    c_request.add_column("merchantnote")
    c_request.add_column("systemnote")
    c_request.add_column("channel")
    c_request.add_column("payoutdate")
    c_request.add_column("loggroupid")
    response = c_request.send
    response
  end

  session = Papapi::Session.new("#{Rails.application.config.pap_server_url}/scripts/server.php",true).
      login(Rails.application.config.pap_merchant_login, Rails.application.config.pap_merchant_password)


  count_c = 0
  offset = 0
  commissions = []
  while true  do
    result = fetch_commissions(session, offset, 30)
    count_c = result.count
    result.each do |r|
      p r['id']
      commissions.push(r)
    end

    if count_c <= commissions.length
      break
    else
      offset=offset + 30
    end
    sleep 5
  end
end