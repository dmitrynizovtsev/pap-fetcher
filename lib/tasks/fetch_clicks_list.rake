task :fetch_clicks_list => :environment do
  require 'papapi'


#{"C":"Gpf_Rpc_Server", "M":"run", "requests":[{"C":"Pap_Merchants_Reports_ClicksGrid", "M":"getRows"
#  , "sort_col":"datetime", "sort_asc":false, "offset":0, "limit":30, "columns":[["id"],["id"],["clickid"
#  ],["firstname"],["lastname"],["userid"],["username"],["banner"],["campaign"],["parentbanner"],["countrycode"
#  ],["rtype"],["datetime"],["refererurl"],["ip"],["useragent"],["channelname"],["cdata1"],["cdata2"]]}
#  ], "S":"11885dc8cbcad426f811d557e1a88901"}

  def fetch_clicks_list(session, offset, limit)
    c_l_request = Papapi::GridRequest.new("Pap_Merchants_Reports_ClicksGrid", "getRows", session)
    c_l_request.sort_col = "datetime"
    c_l_request.sort_asc = false
    c_l_request.offset = offset
    c_l_request.limit = limit
    c_l_request.add_column("id")
    c_l_request.add_column("clickid")
    c_l_request.add_column("firstname")
    c_l_request.add_column("lastname")
    c_l_request.add_column("userid")
    c_l_request.add_column("username")
    c_l_request.add_column("banner")
    c_l_request.add_column("campaign")
    c_l_request.add_column("parentbanner")
    c_l_request.add_column("countrycode")
    c_l_request.add_column("rtype")
    c_l_request.add_column("datetime")
    c_l_request.add_column("refererurl")
    c_l_request.add_column("ip")
    c_l_request.add_column("useragent")
    c_l_request.add_column("channelname")
    c_l_request.add_column("cdata1")
    c_l_request.add_column("cdata2")

    response = c_l_request.send
    response
  end

  session = Papapi::Session.new("#{Rails.application.config.pap_server_url}/scripts/server.php",true).
      login(Rails.application.config.pap_merchant_login, Rails.application.config.pap_merchant_password)


  count_c_l = 0
  offset = 0
  clicks_list = []
  while true  do
    result = fetch_clicks_list(session, offset, 30)
    count_c_l = result.count
    result.each do |r|
      p r['id']
      clicks_list.push(r)
    end

    if count_c_l <= clicks_list.length
      break
    else
      offset=offset + 30
    end
    sleep 5
  end
end